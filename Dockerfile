# s3tools/s3cmd#1182 is still a thing, and i'm **still** waiting for their aforementioned fix.
FROM python:3.8.11-alpine3.14

RUN pip install s3cmd

ENTRYPOINT ["s3cmd"]
